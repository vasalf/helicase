/**
 * helicase
 * Copyright (C) 2018  Vasiliy Alferov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <helicase/mutation/MutationEngine.h>
#include <helicase/cross/CrossEngine.h>
#include <helicase/lib/TemplateChecks.h>
#include <helicase/Params.h>

#include <vector>
#include <random>
#include <algorithm>

namespace Helicase {
    template<class TIndividual, class TRndEng>
    std::vector<std::shared_ptr<TIndividual>> evolve(const std::vector<std::shared_ptr<TIndividual>>& population, Params<TIndividual, TRndEng>& params) {
        static_assert(isIndividual<TIndividual>::value, "Helicase::evolve requires an individual as template argument");

        int size = population.size();
        int alivers = params.alivers;

        if (size == 0) {
            return population;
        }

        std::vector<double> fitness(size);
        std::transform(
            population.begin(), 
            population.end(), 
            fitness.begin(), 
            [](std::shared_ptr<TIndividual> indPtr) -> double { return indPtr->getFitness(); }
        );

        std::vector<double> fitnessPartialSums(size);
        std::partial_sum(fitness.begin(), fitness.end(), fitnessPartialSums.begin());
        std::uniform_real_distribution<double> fitnessDist(0, fitnessPartialSums.back());

        std::vector<int> ind(size);
        std::iota(ind.begin(), ind.end(), 0);
        std::nth_element(ind.begin(), ind.begin() + alivers, ind.end(), [&fitness](int i, int j) -> bool { return fitness[i] > fitness[j]; });

        std::vector<std::shared_ptr<TIndividual>> ret(size);
        
        std::transform(
            ind.begin(), 
            ind.begin() + alivers,
            ret.begin(),
            [&population, &params](int i) -> std::shared_ptr<TIndividual> { return params.mutationEngine->affect(population[i]); }
        );

        std::generate(
            ret.begin() + alivers,
            ret.end(),
            [&population, &fitnessPartialSums, &fitnessDist, &params]() -> std::shared_ptr<TIndividual> {
                double fatherVal = fitnessDist(params.rndEng);
                int father = std::lower_bound(fitnessPartialSums.begin(), fitnessPartialSums.end(), fatherVal) - fitnessPartialSums.begin();
                double motherVal = fitnessDist(params.rndEng);
                int mother = std::lower_bound(fitnessPartialSums.begin(), fitnessPartialSums.end(), motherVal) - fitnessPartialSums.begin();
                return params.mutationEngine->affect(params.crossEngine->cross(population[father], population[mother]));
            }
        );

        return ret;
    }
}
