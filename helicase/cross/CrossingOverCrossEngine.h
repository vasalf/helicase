/**
 * helicase
 * Copyright (C) 2018  Vasiliy Alferov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <helicase/cross/CrossEngine.h>
#include <helicase/lib/TemplateChecks.h>

#include <random>

namespace Helicase {
    template<class TRndEng, class TIndividual>
    class CrossingOverCrossEngine : public CrossEngine<TIndividual> {
        static_assert(isBinaryConvertibleIndividual<TIndividual>::value,
                      "CrossingOverCrossEngine requires binary convertible individuals");
        TRndEng rndEng_;
        int rep_;
      public:
        typedef typename CrossEngine<TIndividual>::TIndividualPtr TIndividualPtr;
        CrossingOverCrossEngine(TRndEng rndEng, int rep) : rndEng_(rndEng), rep_(rep) {}

        virtual TIndividualPtr cross(TIndividualPtr father, TIndividualPtr mother) override {
            BitString lt = father->getBinaryGenotype(), rt = mother->getBinaryGenotype();
            std::uniform_int_distribution<int> dist(0, father->getBinaryGenotypeSize() - 1);
            for (int i = 0; i < rep_; i++) {
                int l = dist(rndEng_);
                int r = dist(rndEng_);
                if (l > r) {
                    std::swap(l, r);
                }
                lt.swapSegments(rt, l, r);
            }
            return std::make_shared<TIndividual>(lt);
        }
    };
}
