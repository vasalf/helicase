/**
 * helicase
 * Copyright (C) 2018  Vasiliy Alferov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <helicase/lib/TemplateChecks.h>
#include <helicase/mutation/MutationEngine.h>

#include <random>

namespace Helicase {
    template<class TRndEng, class TIndividual>
    class BitFlipMutationEngine : public MutationEngine<TIndividual> {
        static_assert(isBinaryConvertibleIndividual<TIndividual>::value,
                      "BitFlipMutationEngine requires binary convertible individuals");
        TRndEng rndEng_;
        double rate_;
        std::uniform_real_distribution<double> dist_;
    public:
        typedef typename MutationEngine<TIndividual>::TIndividualPtr TIndividualPtr;
        BitFlipMutationEngine(TRndEng rndEng, double rate) : rndEng_(rndEng), rate_(rate), dist_(0, 1) {}

        virtual TIndividualPtr affect(TIndividualPtr ind) override {
            int genotypeSize = ind->getBinaryGenotypeSize();
            BitString genotype = ind->getBinaryGenotype();

            for (int i = 0; i < genotypeSize; i++) {
                if (genFlipEvent()) {
                    genotype[i].flip();
                }
            }

            return std::make_shared<TIndividual>(genotype);
        }

    private:
        bool genFlipEvent() {
            return dist_(rndEng_) < rate_;
        }
    };
}
