/**
 * helicase
 * Copyright (C) 2018  Vasiliy Alferov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <helicase/lib/BitString.h>

namespace Helicase {
    template<typename TIndividual>
    class IsIndividual {
    private:
        using Yes = char[2];
        using No = char[1];
        
        template<typename U, double (U::*)() const>
        struct CheckGetFitnessMethod;
        template<typename U>
        static Yes& hasGetFitnessMethod(CheckGetFitnessMethod<U, &U::getFitness>*);
        template<typename U>
        static No& hasGetFitnessMethod(...);
    public:
        static constexpr bool hasGetFitness = sizeof(hasGetFitnessMethod<TIndividual>(nullptr)) == sizeof(Yes);
        static constexpr bool result = hasGetFitness;
    };

    template<typename TIndividual>
    struct isIndividual : public std::integral_constant<bool, IsIndividual<TIndividual>::result> {};

    template<typename TIndividual>
    class IsBinaryConvertible {
        using Yes = char[2];
        using No = char[1];

        template<typename U, BitString (U::*)() const>
        struct CheckHasGetBinaryGenotypeMethod;
        template<typename U>
        static Yes& hasGetBinaryGenotypeMethod(CheckHasGetBinaryGenotypeMethod<U, &U::getBinaryGenotype>*);
        template<typename U>
        static No& hasGetBinaryGenotypeMethod(...);
        
        template<typename U, int (U::*)() const>
        struct CheckHasGetBinaryGenotypeSizeMethod;
        template<typename U>
        static Yes& hasGetBinaryGenotypeSizeMethod(CheckHasGetBinaryGenotypeSizeMethod<U, &U::getBinaryGenotypeSize>*);
        template<typename U>
        static No& hasGetBinaryGenotypeSizeMethod(...);
    public:
        static constexpr bool hasGetBinaryGenotype = sizeof(hasGetBinaryGenotypeMethod<TIndividual>(nullptr)) == sizeof(Yes);
        static constexpr bool hasGetBinaryGenotypeSize = sizeof(hasGetBinaryGenotypeSizeMethod<TIndividual>(nullptr)) == sizeof(Yes);
        static constexpr bool hasBitStringConstructor = std::is_constructible<TIndividual, const BitString&>::value;
        static constexpr bool result = isIndividual<TIndividual>::value 
                                         && hasGetBinaryGenotype 
                                         && hasGetBinaryGenotypeSize
                                         && hasBitStringConstructor;
    };

    template<typename TIndividual>
    struct isBinaryConvertibleIndividual : public std::integral_constant<bool, IsBinaryConvertible<TIndividual>::result> {};
}
