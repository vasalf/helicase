/**
 * helicase
 * Copyright (C) 2018  Vasiliy Alferov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <vector>
#include <cstdint>

namespace Helicase {
    class BitString {
        std::size_t length_;
        std::vector<std::uint64_t> content_;
    public:
        BitString(std::size_t length, int bit = 0);

        class BitReference {
            std::uint64_t& where_;
            std::size_t which_;
        public:
            BitReference(std::uint64_t& where, std::size_t which)
                : where_(where), which_(which) {}
            BitReference(BitReference&&) = default;

            BitReference& operator=(bool value);
            BitReference& operator=(const BitReference& ref);
            operator bool() const;

            BitReference& flip();
        };

        bool operator[](std::size_t at) const;
        BitReference operator[](std::size_t at);
        std::size_t size() const;
        void swapSegments(BitString& with, std::size_t l, std::size_t r);

        bool operator==(const BitString& other) const;
        bool operator!=(const BitString& other) const;
    };
}
