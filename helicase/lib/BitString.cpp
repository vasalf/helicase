/**
 * helicase
 * Copyright (C) 2018  Vasiliy Alferov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "BitString.h"
#include <iostream>

using namespace Helicase;

Helicase::BitString::BitString(std::size_t length, int bit) {
    length_ = length;
    content_.resize((length + 63) / 64, bit ? ~0 : 0);  
}

BitString::BitReference& Helicase::BitString::BitReference::operator=(bool value) {
    if (value) {
        where_ |= 1ull << (std::uint64_t)which_;    
    } else {
        where_ &= ~(1ull << (std::uint64_t)which_);
    }

    return *this;
}

BitString::BitReference& Helicase::BitString::BitReference::operator=(const Helicase::BitString::BitReference& ref) {
    return *this = (bool)ref;
}

Helicase::BitString::BitReference::operator bool() const {
    return (where_ >> (std::uint64_t)which_) & 1;
}

BitString::BitReference& Helicase::BitString::BitReference::flip() {
    if ((where_ >> (std::uint64_t)which_) & 1) {
        where_ &= ~(1ull << (std::uint64_t)which_);
    } else {
        where_ |= 1ull << (std::uint64_t)which_;
    }
}

bool Helicase::BitString::operator[](std::size_t at) const {
    return (content_[at / 64] >> (uint64_t)(at % 64)) & 1;
}

BitString::BitReference Helicase::BitString::operator[](std::size_t at) {
    return BitString::BitReference(content_[at / 64], at % 64);
}

std::size_t BitString::size() const {
    return length_;
}

namespace {
    inline std::uint64_t getMaskSuffix(std::size_t r) {
        return r == 63 ? ~0 : (1ull << (std::uint64_t)(r + 1)) - 1;
    }

    inline std::uint64_t getSwapMask(std::size_t l, std::size_t r) {
        return getMaskSuffix(r) - getMaskSuffix(l - 1);
    }

    inline std::uint64_t swapInInt(std::uint64_t in, std::uint64_t with, std::size_t l, std::size_t r) {
        std::uint64_t mask = getSwapMask(l, r);
        return (in & (~mask)) | (with & mask);  
    }
}

void BitString::swapSegments(BitString& with, std::size_t l, std::size_t r) {
    std::size_t ls = l / 64;
    std::size_t rs = r / 64;
    for (std::size_t i = ls; i != rs + 1; i++) {
        content_[i] = swapInInt(content_[i], with.content_[i], std::max(64 * i, l) - 64 * i, std::min(64 * i + 63, r) - 64 * i);
    }
}

bool BitString::operator==(const BitString& other) const {
    if (length_ != other.length_) {
        return false;
    }
    for (int i = 0; i < content_.size(); i++) {
        if (i * 64 + 63 < length_) {
            if (content_[i] != other.content_[i])
                return false;
        } else {
            std::uint64_t masklen = length_ - 64 * i;
            std::uint64_t mask = (1ull << masklen) - 1;
            if ((content_[i] & mask) != (other.content_[i] & mask))
                return false; 
        }
    }
    return true;
}

bool BitString::operator!=(const BitString& other) const {
    return !(*this == other);
}
