/**
 * helicase
 * Copyright (C) 2018  Vasiliy Alferov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gtest/gtest.h>

#include <helicase/cross/CrossEngine.h>
#include <helicase/cross/CrossingOverCrossEngine.h>

#include "Common.h"

#include <random>

using namespace Helicase;
using namespace HelicaseTest;

namespace {
    bool diffIsSegment(const BitString& lt, const BitString& rt) {
        int nd = 0, fd = -1, ld = -1;
        for (int i = 0; i < lt.size(); i++) {
            if (lt[i] != rt[i]) {
                nd++;
                ld = i;
                if (fd == -1)
                    fd = i;
            }
        }
        return nd > 0 && nd == ld - fd + 1;
    }
}

TEST(TestCrossingOverCrossing, testZeroCrossingsDoingNothing) {
    std::mt19937 rnd;
    BitString fatherGenotype(179, 0), motherGenotype(179, 1);

    CrossEnginePtr<GetSetGenotypeIndividual> eng
        = std::make_shared<CrossingOverCrossEngine<std::mt19937, GetSetGenotypeIndividual> > (std::mt19937(239), 0);
    std::shared_ptr<GetSetGenotypeIndividual> father
        = std::make_shared<GetSetGenotypeIndividual>(fatherGenotype);
    std::shared_ptr<GetSetGenotypeIndividual> mother
        = std::make_shared<GetSetGenotypeIndividual>(motherGenotype);
    
    std::shared_ptr<GetSetGenotypeIndividual> child = eng->cross(father, mother);
    BitString childGenotype = child->getBinaryGenotype();

    ASSERT_EQ(childGenotype, fatherGenotype);
}

TEST(TestCrossingOverCrossing, testOneCrossingCrossingNonEmptySegment) {
    std::mt19937 rnd;

    BitString fatherGenotype(200179, 0), motherGenotype(200179, 1);

    
    CrossEnginePtr<GetSetGenotypeIndividual> eng
        = std::make_shared<CrossingOverCrossEngine<std::mt19937, GetSetGenotypeIndividual> > (std::mt19937(239), 1);
    std::shared_ptr<GetSetGenotypeIndividual> father
        = std::make_shared<GetSetGenotypeIndividual>(fatherGenotype);
    std::shared_ptr<GetSetGenotypeIndividual> mother
        = std::make_shared<GetSetGenotypeIndividual>(motherGenotype);
    
    for (int i = 0; i < 100; i++) {
        std::shared_ptr<GetSetGenotypeIndividual> child = eng->cross(father, mother);
        BitString childGenotype = child->getBinaryGenotype();
        ASSERT_TRUE(diffIsSegment(childGenotype, fatherGenotype));
    }
}
