/**
 * helicase
 * Copyright (C) 2018  Vasiliy Alferov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gtest/gtest.h>

#include <helicase/cross/CrossEngine.h>
#include <helicase/mutation/MutationEngine.h>
#include <helicase/mutation/DoNothingMutationEngine.h>
#include <helicase/Params.h>
#include <helicase/Evolve.h>

#include <memory>
#include <random>

using namespace Helicase;

struct BoolDoubleIndividual {
    bool bool_v;
    double double_v;

    BoolDoubleIndividual(bool bv, double dv) : bool_v(bv), double_v(dv) {}

    double getFitness() const {
        return double_v;
    }
};

class BDICross : public CrossEngine<BoolDoubleIndividual> {
public:
    typedef std::shared_ptr<BoolDoubleIndividual> TIndividualPtr;

    virtual TIndividualPtr cross(TIndividualPtr father, TIndividualPtr mother) override {
        return std::make_shared<BoolDoubleIndividual>(false, father->double_v);
    }
};

TEST(TestEvolve, testBestIndividualsAreSelectedForEvolution) {
    Params<BoolDoubleIndividual, std::mt19937> params {
        .crossEngine = std::make_shared<BDICross>(),
        .mutationEngine = std::make_shared<DoNothingMutationEngine<BoolDoubleIndividual>>(),
        .alivers = 60,
        .rndEng = std::mt19937()
    };
    
    std::vector<std::shared_ptr<BoolDoubleIndividual>> population;
    for (int i = 100; i > 0; i--) {
        population.push_back(std::make_shared<BoolDoubleIndividual>(true, i));
    }
    std::shuffle(population.begin(), population.end(), std::mt19937(179));

    std::vector<std::shared_ptr<BoolDoubleIndividual>> res = evolve(population, params);
    
    int oldCnt = 0;
    for (auto indPtr : res) {
        if (indPtr->bool_v) {
            oldCnt++;
            ASSERT_GT(indPtr->double_v, 40);
        }
    }

    ASSERT_EQ(60, oldCnt);
}

TEST(TestEvolve, testEmptyPopulation) {
    Params<BoolDoubleIndividual, std::mt19937> params {
        .crossEngine = std::make_shared<BDICross>(),
        .mutationEngine = std::make_shared<DoNothingMutationEngine<BoolDoubleIndividual>>(),
        .alivers = 0,
        .rndEng = std::mt19937()
    };

    std::vector<std::shared_ptr<BoolDoubleIndividual>> population;
    auto result = Helicase::evolve(population, params);

    ASSERT_TRUE(result.empty());
}
