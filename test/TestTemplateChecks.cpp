/**
 * helicase
 * Copyright (C) 2018  Vasiliy Alferov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gtest/gtest.h>
#include <helicase/lib/TemplateChecks.h>

using namespace Helicase;

class EmptyClass {};

TEST(TestTemplateChecks, testEmptyIndividual) {
    ASSERT_FALSE(isIndividual<EmptyClass>::value);
}

class CorrectIndividual {
public:
    double getFitness() const;
};

TEST(TestTemplateChecks, testCorrectIndividual) {
    ASSERT_TRUE(isIndividual<CorrectIndividual>::value);
}

class NonConstFunctionIndividual {
public:
    double getFitness();
};

TEST(TestTemplateChecks, testNonConstFunctionIndividual) {
    ASSERT_FALSE(isIndividual<NonConstFunctionIndividual>::value);
}

class WrongReturnTypeIndividual {
public:
    char getFitness() const;
};

TEST(TestTemplateChecks, testWrongReturnTypeIndividual) {
    ASSERT_FALSE(isIndividual<WrongReturnTypeIndividual>::value);
}

TEST(TestTemplateChecks, testNonBinaryConvertibleIndividual) {
    ASSERT_FALSE(isBinaryConvertibleIndividual<CorrectIndividual>::value);
}

class BinaryConvertibleIndividual {
public:
    double getFitness() const;
    
    int getBinaryGenotypeSize() const;
    BitString getBinaryGenotype() const;

    BinaryConvertibleIndividual(const BitString&);
};

TEST(TestTemplateChecks, testBinaryConvertibleIndividual) {
    ASSERT_TRUE(isBinaryConvertibleIndividual<BinaryConvertibleIndividual>::value);
}

class BinaryConvertibleNotIndividual {
public:
    int getBinaryGenotypeSize() const;
    BitString getBinaryGenotype() const;

    BinaryConvertibleNotIndividual(const BitString&);
};

TEST(TestTemplateChecks, testBinaryConvertibleNotIndividual) {
    ASSERT_FALSE(isBinaryConvertibleIndividual<BinaryConvertibleNotIndividual>::value);
}

class NoGetBinaryGenotypeSizeFunctionBCIndividual {
public:
    double getFitness() const;
    
    BitString getBinaryGenotype() const;

    NoGetBinaryGenotypeSizeFunctionBCIndividual(const BitString&);
};

TEST(TestTemplateChecks, testNoGetBinaryGenotypeSizeFunctionBinaryConvertibleIndividual) {
    ASSERT_FALSE(isBinaryConvertibleIndividual<NoGetBinaryGenotypeSizeFunctionBCIndividual>::value);
}

class NoGetBinaryGenotypeFunctionBCIndividual {
public:
    typedef int TGenotype;

    const int& getGenotype() const;
    double getFitness() const;
    
    int getBinaryGenotypeSize() const;

    NoGetBinaryGenotypeFunctionBCIndividual(const BitString&);
};

TEST(TestTemplateChecks, testNoGetBinaryGenotypeFunctionBinaryConvertibleIndividual) {
    ASSERT_FALSE(isBinaryConvertibleIndividual<NoGetBinaryGenotypeFunctionBCIndividual>::value);
}

class NoBitStringConstructorBCIndividual {
public:
    double getFitness() const;
    
    int getBinaryGenotypeSize() const;
    BitString getBinaryGenotype() const;
};

TEST(TestTemplateChecks, testNoBitStringConstructorBinaryConvertibleIndividual) {
    ASSERT_FALSE(isBinaryConvertibleIndividual<NoBitStringConstructorBCIndividual>::value);
}

class NoReferenceInConstructorBCIndividual {
public:
    double getFitness() const;

    int getBinaryGenotypeSize() const;
    BitString getBinaryGenotype() const;

    NoReferenceInConstructorBCIndividual(BitString);
};

TEST(TestTemplateChecks, testNoReferenceInConstructorBinaryConvertibleIndividual) {
    ASSERT_TRUE(isBinaryConvertibleIndividual<NoReferenceInConstructorBCIndividual>::value);
}

class NonConstFunctionsBinaryConvertibleIndividual {
public:
    double getFitness() const;

    int getBinaryGenotypeSize();
    BitString getBinaryGenotype();

    NonConstFunctionsBinaryConvertibleIndividual(const BitString&);
};

TEST(TestTemplateChecks, testNonConstFunctionsBinaryConvertibleIndividual) {
    ASSERT_FALSE(isBinaryConvertibleIndividual<NonConstFunctionsBinaryConvertibleIndividual>::value);
}
