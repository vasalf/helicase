/**
 * helicase
 * Copyright (C) 2018  Vasiliy Alferov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gtest/gtest.h>
#include <helicase/mutation/BitFlipMutationEngine.h>

#include "Common.h"

using namespace Helicase;
using namespace HelicaseTest;

TEST(TestBitFlipMutations, testZeroProbability) {
    std::mt19937 rnd;
    BitString bs(20179);

    for (int i = 0; i < bs.size(); i++) {
        bs[i] = rnd() % 2;
    }

    MutationEnginePtr<GetSetGenotypeIndividual> eng 
        = std::make_shared<BitFlipMutationEngine<std::mt19937, GetSetGenotypeIndividual> >(std::mt19937(239), 0);
    std::shared_ptr<GetSetGenotypeIndividual> ind = std::make_shared<GetSetGenotypeIndividual>(bs);

    for (int i = 0; i < 100; i++) {
        ind = eng->affect(ind);
        BitString newGenotype = ind->getBinaryGenotype();
        for (int i = 0; i < ind->getBinaryGenotypeSize(); i++) {
            ASSERT_EQ(bs[i], newGenotype[i]);
        }
    }
}

TEST(TestBitFlipMutations, testFullProbability) {
    std::mt19937 rnd;
    BitString bs(20179);

    for (int i = 0; i < bs.size(); i++) {
        bs[i] = rnd() % 2;
    }

    MutationEnginePtr<GetSetGenotypeIndividual> eng
        = std::make_shared<BitFlipMutationEngine<std::mt19937, GetSetGenotypeIndividual> >(std::mt19937(239), 1);
    std::shared_ptr<GetSetGenotypeIndividual> ind = std::make_shared<GetSetGenotypeIndividual>(bs);

    for (int t = 0; t < 100; t++) {
        ind = eng->affect(ind);
        BitString newGenotype = ind->getBinaryGenotype();
        for (int i = 0; i < ind->getBinaryGenotypeSize(); i++) {
            ASSERT_NE(bs[i], newGenotype[i]);
        }
        bs = newGenotype;
    }
}
