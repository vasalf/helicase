/**
 * helicase
 * Copyright (C) 2018  Vasiliy Alferov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gtest/gtest.h>
#include <helicase/lib/BitString.h>

#include <iostream>
#include <string>
#include <random>

using namespace Helicase;

namespace {
    void testFromSizeConstruction(std::size_t size) {
        BitString bs(size);
        for (std::size_t i = 0; i != size; i++) {
            ASSERT_FALSE(bs[i]);
        }
    }

    void testFromSizeSetConstruction(std::size_t size) {
        BitString bs(size, 1);
        for (std::size_t i = 0; i != size; i++) {
            ASSERT_TRUE(bs[i]);
        }
    }
}

TEST(TestBitString, testFromSizeConstruction) {
    testFromSizeConstruction(5);
}

TEST(TestBitString, testLargeStringFromSizeConstruction) {
    testFromSizeConstruction(200179);
}

TEST(TestBitString, testFromSizeSetConstruction) {
    testFromSizeSetConstruction(5);
}

TEST(TestBitString, testLargeStringFromSizeSetConstruction) {
    testFromSizeSetConstruction(200179);
}

TEST(TestBitString, testGetSetValues) {
    std::mt19937 rnd(179);
    std::uniform_int_distribution<int> dist(0, 1);

    std::string real;
    real.resize(65);
    BitString bs(65);
    for (int i = 0; i < 65; i++) {
        int v = dist(rnd);
        real[i] = '0' + v;
        bs[i] = v;
    }

    for (int i = 0; i < 65; i++) {
        ASSERT_EQ(real[i] - '0', bs[i]);
    }
}

TEST(TestBitString, testBitReferenceStorage) {
    BitString bs(1);
    ASSERT_FALSE(bs[0]);
    BitString::BitReference ref = bs[0];
    ref = true;
    ASSERT_TRUE(bs[0]);
    ref = 0;
    ASSERT_FALSE(bs[0]);
}

TEST(TestBitString, testBitReferenceChaining) {
    BitString bs(5);
    ASSERT_TRUE(bs[0] = bs[1] = bs[2] = bs[3] = bs[4] = true);
    for (int i = 0; i < 5; i++) {
        ASSERT_TRUE(bs[i]);
    }
}

TEST(TestBitString, testBitReferenceFlip) {
    std::mt19937 rnd(179);
    std::uniform_int_distribution<int> dist(0, 1);
    
    std::string real;
    real.resize(200179);
    BitString bs(200179);
    for (int i = 0; i < 200179; i++) {
        int v = dist(rnd);
        real[i] = '0' + v;
        bs[i] = v;
    }

    for (int i = 0; i < 200179; i++) {
        bs[i].flip();
    }
    for (int i = 0; i < 200179; i++) {
        ASSERT_NE(real[i] - '0', bs[i]); 
    }
    for (int i = 0; i < 200179; i++) {
        bs[i].flip();
    }
    for (int i = 0; i < 200179; i++) {
        ASSERT_EQ(real[i] - '0', bs[i]); 
    }
}

TEST(TestBitString, testBitStringSize) {
    BitString bs(65);
    ASSERT_EQ(65, bs.size());
}

namespace {
    void testSwapSegments(std::size_t size, std::size_t l, std::size_t r) {
        BitString in(size);
        BitString with(size, 1);

        in.swapSegments(with, l, r);
        
        for (int i = 0; i < size; i++) {
            ASSERT_EQ(in[i], l <= i && i <= r);
        }
    }
}

TEST(TestBitString, testSwapSegsInSmallString) {
    testSwapSegments(5, 2, 3);
}

TEST(TestBitString, testSwapSegmentsOfLengthOne) {
    testSwapSegments(5, 2, 2);
}

TEST(TestBitString, testSwapSegmentsAdjacentInts) {
    testSwapSegments(179, 27, 80);
}

TEST(TestBitString, testSwapSegmentsNonAdjacentInts) {
    testSwapSegments(179, 27, 130);
}

TEST(TestBitString, testSwapSegmentsStartingWithIntStart) {
    testSwapSegments(179, 64, 130);
}

TEST(TestBitString, testSwapSegmentsEndingWithIntEnd) {
    testSwapSegments(179, 27, 127);
}

TEST(TestBitString, testSwapSegmentsStartingWithZero) {
    testSwapSegments(179, 0, 130);
}

TEST(TestBitString, testSwapSegmentsEndingWithLast) {
    testSwapSegments(179, 27, 178);
}

TEST(TestBitString, testSwapSegmentsEndingWithLastInDivisibleByBase) {
    testSwapSegments(512, 27, 511);
}

TEST(TestBitString, testSwapSegmentsFullSwap) {
    testSwapSegments(179, 0, 178);
}

TEST(TestBitString, testSwapSegmentsFullSwapDivisibleByBase) {
    testSwapSegments(512, 0, 511);
}

TEST(TestBitString, testSwapSegmentsEveryCaseOfSmallLength) {
    for (int size = 1; size <= 128; size++) {
        for (int l = 0; l < size; l++) {
            for (int r = l; r < size; r++) {
                testSwapSegments(size, l, r);
            }
        }
    }
}

namespace {
    void shouldBeEqual(const BitString& lt, const BitString& rt) {
        ASSERT_TRUE(lt == rt);
        ASSERT_FALSE(lt != rt);
    }

    void shouldNotBeEqual(const BitString& lt, const BitString& rt) {
        ASSERT_FALSE(lt == rt);
        ASSERT_TRUE(lt != rt);
    }
}

TEST(TestBitString, testEqualityOfEmptyStrings) {
    shouldBeEqual(BitString(0), BitString(0));
}

TEST(TestBitString, testNonEqualityOfStringsOfDifferentLengths) {
    shouldNotBeEqual(BitString(4), BitString(5));
}

TEST(TestBitString, testEqualityOfEqualRandomStrings) {
    std::mt19937 rnd(179);
    BitString a(239179), b(239179);
    for (int i = 0; i < 239179; i++) {
        a[i] = b[i] = rnd() % 2;
    }

    shouldBeEqual(a, b);
}

TEST(TestBitString, testEqualityOfCreatedUnequalEqualRandomStrings) {
    std::mt19937 rnd(179);
    BitString a(239179), b(239179, 1);
    for (int i = 0; i < 239179; i++) {
        a[i] = b[i] = rnd() % 2;
    }

    shouldBeEqual(a, b);
}

TEST(TestBitString, testEqualityOfCopiedString) {
    std::mt19937 rnd(179);
    BitString a(239179);
    for (int i = 0; i < 239179; i++) {
        a[i] = rnd() % 2;
    }
    BitString b = a;

    shouldBeEqual(a, b);
}

TEST(TestBitString, testNonEqualStringsOfEqualLengths) {
    shouldNotBeEqual(BitString(138, 0), BitString(138, 1));
}

TEST(TestBitString, testNonEqualStringsOfEqualSmallLenghts) {
    BitString lt(5, 0);
    lt[4] = 1;
    shouldNotBeEqual(lt, BitString(5, 0));
}
