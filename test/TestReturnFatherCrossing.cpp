/**
 * helicase
 * Copyright (C) 2018  Vasiliy Alferov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gtest/gtest.h>

#include <helicase/cross/CrossEngine.h>
#include <helicase/cross/ReturnFatherCrossEngine.h>

#include "Common.h"

#include <random>

using namespace Helicase;
using namespace HelicaseTest;

TEST(TestReturnFatherCrosing, testReturnFatherCrossing) {
    std::mt19937 rnd(179);
    BitString fatherGenotype(239), motherGenotype(239);
    for (int i = 0; i < 239; i++) {
        fatherGenotype[i] = rnd() % 2;
        motherGenotype[i] = rnd() % 2;
    }

    CrossEnginePtr<GetSetGenotypeIndividual> eng
        = std::make_shared<ReturnFatherCrossEngine<GetSetGenotypeIndividual>>();
    std::shared_ptr<GetSetGenotypeIndividual> father
        = std::make_shared<GetSetGenotypeIndividual>(fatherGenotype);
    std::shared_ptr<GetSetGenotypeIndividual> mother
        = std::make_shared<GetSetGenotypeIndividual>(motherGenotype);

    std::shared_ptr<GetSetGenotypeIndividual> child = eng->cross(father, mother);
    BitString childGenotype = child->getBinaryGenotype();

    ASSERT_EQ(childGenotype, fatherGenotype);
}
